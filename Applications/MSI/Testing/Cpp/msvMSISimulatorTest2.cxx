
#include<sstream>

#include<vtkSmartPointer.h>
#include<vtkPolyData.h>
#include<vtkXMLPolyDataReader.h>
#include<vtkXMLPolyDataWriter.h>
#include<vtkXMLHierarchicalBoxDataWriter.h>
#include<vtkHierarchicalBoxDataSet.h>
#include <vtkTestUtilities.h>

#include "msvFluidSimulator.h"
// Headers for basic PETSc functions
#include <petscsys.h>


int msvMSISimulatorTest2(int ac, char **av)
{
  // Get the data test files
  const char* in_file =
  vtkTestUtilities::ExpandDataFileName(ac,av,"amr_lagrangian_dataset.vtp");
  const char* out_file =
  vtkTestUtilities::ExpandDataFileName(ac,av,"amr_grid_dataset.");
  const char* in_database = "Resources/simulator.config";
  
  vtkSmartPointer<vtkXMLPolyDataReader> reader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
  vtkSmartPointer<vtkXMLHierarchicalBoxDataWriter> writer = vtkSmartPointer<vtkXMLHierarchicalBoxDataWriter>::New();
  vtkSmartPointer<vtkHierarchicalBoxDataSet> dataset = vtkSmartPointer<vtkHierarchicalBoxDataSet>::New();
  
  
  reader->SetFileName(in_file);
  reader->Update();
  
  vtkPolyData *data = reader->GetOutput();
  
  PetscInitialize(&ac,&av,PETSC_NULL,PETSC_NULL);
  {
    msvFluidSimulator simulator;
    simulator.msvInitializeAMR(in_database,4,5,data);
    simulator.AmrToVTK(dataset);
    // AMR writer
    writer->SetInput(simulator.getAMRDataSet());
    writer->SetDataModeToBinary();
    std::string amrfilename(out_file);
    amrfilename += writer->GetDefaultFileExtension();
    writer->SetFileName(amrfilename.c_str());
    writer->Write();    
  }
  PetscFinalize();
  
  return 0;
}